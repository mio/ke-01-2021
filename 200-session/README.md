## Planning and Running a deployment of a stateless app on Kubernetes

### Lab structure:
1. Satisfy perquisites - general background instructions / references
2. Generate Manifests
3. Deploy to kubernetes
4. Test deployment

  ### 1. perquisites

  - You understand what a kubernetes `deployment` is [link](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
  - You understand what `services` are and the different `service` types [link](https://kubernetes.io/docs/concepts/services-networking/service/)
  - `minikube` or equivalent `kuberntes` cluster [Link to minikube getting started]()

    ### 1.1 msa-demo-app
    This is the application we are going to deploy in this lab
    - 4 components
      - redis cache
      - msa-api - a small express app (node.js) which stores pings in redis
      - msa-pinger - send pings to the msa-api
      - msa-poller - reads from msa-api current ping count
    - 2 serve (provide an interface to other services e.g database, api)
    - 2 being served (do not need a service. Only consume)
    In this lab we will be creating 1 `namespace `these 4 `deployment`s and 2 `service`s and run our demo-app on kubernetes.

    #### 1.2 Docker images we will be using for this lab:
    1. msa-demo namespace
    1. redis
    1. shelleg/msa-api:config
    1. shelleg/msa-pinger:latest
    1. shelleg/msa-poller:latest

    #### 1.3 Listen ports - a.k.a `services`
    1. `redis` will be using its default port `6379`
    1. `msa-api` will listen on port `8080`

  ### 2. Manifests we will create and use in this lab
  - msa-demo-ns.yml
  - redis.yml
  - msa-api.yml
  - msa-pinger.yml
  - msa-poller.yml

    **please note:** You can use the .reference directory herein as reference.)

    ##### 2.1 Create `msa-demo` namespace 

    ```sh
    kubectl create namespace msa-demo
    ```
    Verify namespace creation:
    ```sh 
    kubectl get namespace
    ```
    Should yield:
    ```sh
    NAME              STATUS   AGE
    default           Active   11m
    kube-node-lease   Active   11m
    kube-public       Active   11m
    kube-system       Active   11m
    msa-demo          Active   8s
    ```

    ##### 2.2.1 Create deployment manifest for redis (using kubectl --dry-run)

    ```sh
    kubectl  create deployment redis --image=redis:5 --port=6379 --dry-run=client -o yaml > redis-deployment.yaml
    ```

    which yields:
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      creationTimestamp: null
      labels:
        app: redis
      name: redis
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: redis
      strategy: {}
      template:
        metadata:
          creationTimestamp: null
          labels:
            app: redis
        spec:
          containers:
          - image: redis:5
            name: redis
            ports:
            - containerPort: 6379
            resources: {}
    status: {}
    ```
    
    ##### 2.2.2 Create service manifest for redis (using kubectl --dry-run)

    ```sh
    kubectl  create service clusterip redis  --tcp=6379 --dry-run=client  -oyaml > redis-service.yaml
    ```

    which yields:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      creationTimestamp: null
      labels:
        app: redis
      name: redis
    spec:
      ports:
      - name: "6379"
        port: 6379
        protocol: TCP
        targetPort: 6379
      selector:
        app: redis
      type: ClusterIP
    status:
      loadBalancer: {}
    ```
    
    #### 2.3.1 Create deployment manifest for msa-api
    ```sh
    kubectl create deployment msa-api --image=shelleg/msa-api:config  --port=8080 --dry-run=client -o yaml > msa-api-deployment.yaml
    ```

    which yields:
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      creationTimestamp: null
      labels:
        app: msa-api
      name: msa-api
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: msa-api
      strategy: {}
      template:
        metadata:
          creationTimestamp: null
          labels:
            app: msa-api
        spec:
          containers:
          - image: shelleg/msa-api:config
            name: msa-api
      ports:
            - containerPort: 8080
            resources: {}
    status: {}
    ```
    The msa-api pod must be configured to find the cache server. It will look for an environment variable `REDIS_URL`
    Edit the file `msa-api-deployment.yaml`:  In the `containers` section, under `spec`, after the `image` and `name` definition add:
    ```yaml
    env:
      - name: REDIS_URL 
        value: redis://redis:6379
    ```
    Save your changes. Now the pod will know where to find the cache server.
    
    #### 2.3.2 Create service manifest for msa-api
    ```sh
    kubectl  create service clusterip msa-api --tcp=8080 --dry-run=client  -oyaml > msa-api-service.yaml
    ```
    which yield:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      creationTimestamp: null
      labels:
        app: msa-api
      name: msa-api
    spec:
      ports:
      - name: "8080"
        port: 8080
        protocol: TCP
        targetPort: 8080
      selector:
        app: msa-api
      type: ClusterIP
    status:
      loadBalancer: {}
      ```
      
      ### Lets deploy
      run 
      ```sh
      ls
      ```
      You should have this in your folder:
      ```
      msa-api-deployment.yaml  README.md              redis-service.yaml
      msa-api-service.yaml     redis-deployment.yaml
      ```
      Now run:
      ```sh
       kubectl create --namespace msa-demo -f .
       ```
       See the objects you just created:
       ```sh
       kubectl get all --namespace msa-demo
       ```
       
       ### Lets test
       Link the api service to a local port:
       ```sh
       kubectl --namespace msa-demo port-forward service/msa-api 8080
       ```
       point your browser to `http://localhost:8080`, verify you get `Current ping count: null `
       
       Now ping to it by running: (You will need to do it in a new terminal)
       ```sh
       curl -d "" http://localhost:8080/ping
       ```
       Refresh your browser and see if ping was counted.
       (Stop the port-forward with `ctrl-c`)
      

    #### 2.4 Create deployment manifest for msa-pinger
    ```sh
    kubectl create deployment msa-pinger --image=shelleg/msa-pinger:latest --dry-run=client -o yaml > msa-pinger.yaml
    ```
    Which yield:
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      creationTimestamp: null
      labels:
        app: msa-pinger
      name: msa-pinger
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: msa-pinger
      strategy: {}
      template:
        metadata:
          creationTimestamp: null
          labels:
            app: msa-pinger
        spec:
          containers:
          - image: shelleg/msa-pinger:latest
            name: msa-pinger
            resources: {}
    status: {}
    ```
    `msa-pinger` needs 2 environment variables: `API_URL` that will configure the api server location and `DEBUG` to set the debug level.
     Unlike `docker run` command, `kubectl create deployment` can't set environment variables. So we are going to set it in the yaml file.
     Edit the file `msa-pinger.yaml`, add the lines in the same location as before:
     (first and last line in the snippet are given only to provide context and should not be duplicated)
    ```yaml
            name: msa-pinger
            env:
              - name: API_URL
                value: ${MSA_API_SERVICE_HOST}:${MSA_API_SERVICE_PORT}
              - name: DEBUG
                value: "true"
            resources: {}
    ```

    We can use the `MSA_API_SERVICE_HOST` and `MSA_API_SERVICE_PORT` to construct the `API_URL` variable which is expected by the **msa-pinger** pod,
    because we know we have a service named `msa-api` and kubernetes will provide the environment variables. This object must be dployed after the msa-api service was set.
    
    Diploy the deployment object
    ```sh
    kubectl --namespace msa-demo create -f msa-pinger.yaml
    ```

    To see the environment that is generated by kubernetes execute
    ```sh
    kubectl -n msa-demo exec -it `kubectl -n msa-demo get po | grep msa-pinger | awk '{print $1}'` -- printenv | grep MSA
    ```
    (This command select the `msa-pinger` pod from the namespace pod list and execute the command `printenv` in it. The
    output is bing filterd to show lines with the `MSA` string in it.)
    which should yield something like:
    ```sh
    API_URL=${MSA_API_SERVICE_HOST}:${MSA_API_SERVICE_PORT}
    MSA_API_SERVICE_HOST=100.68.245.88
    MSA_API_SERVICE_PORT=8080
    MSA_API_PORT=tcp://100.68.245.88:8080
    MSA_API_PORT_8080_TCP_PORT=8080
    MSA_API_PORT_8080_TCP_ADDR=100.68.245.88
    MSA_API_PORT_8080_TCP=tcp://100.68.245.88:8080
    MSA_API_PORT_8080_TCP_PROTO=tcp`
    ```
    Hence we can use these environment variables in our deployment

    Run the port-forward command again. Refresh your browser few times to see that the pinger is working.
    
    #### 2.5 Create deployment manifeset for msa-poller
    ```sh
    kubectl create deployment msa-poller --image=shelleg/msa-poller:latest --dry-run=client -o yaml > msa-poller.yaml
    ```
    which yields:
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      creationTimestamp: null
      labels:
        run: msa-poller
      name: msa-poller
    spec:
      replicas: 1
      selector:
        matchLabels:
          run: msa-poller
      strategy: {}
      template:
        metadata:
          creationTimestamp: null
          labels:
            run: msa-poller
        spec:
          containers:
            image: shelleg/msa-poller:latest
            name: msa-poller
            resources: {}
    status: {}
    ```
    Again, there is an environment variables to set. 
    By now, you should know what to do. The code to add is the same as pinger's:
    ```yaml
          env:
            - name: API_URL
              value: ${MSA_API_SERVICE_HOST}:${MSA_API_SERVICE_PORT}
    ```
    
    Deploy the poller:
    ```sh
    kubectl --namespace msa-demo create -f msa-poller.yaml
    ```

  ### 3. Verify our system
##### 3.1 validate services
```sh
    kubectl -n msa-demo get svc
```
should yield:
```sh
NAME      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
msa-api   ClusterIP   100.68.245.88   <none>        8080/TCP   4m
  redis     ClusterIP   100.64.28.115   <none>        6379/TCP   4m
```

#### 4.2 validate pods
```sh
     kubectl -n msa-demo get po
```
should yield:
```sh
     NAMESPACE   NAME                          READY     STATUS    RESTARTS   AGE
     msa-demo    msa-api-6cb7c9c6bf-rz6fd      1/1       Running   0          32m
     msa-demo    msa-pinger-599f4c5bf9-s8sc7   1/1       Running   0          20m
     msa-demo    msa-poller-7cfcb4c8d-wlrr6    1/1       Running   0          15m
     msa-demo    redis-685c788858-dw7nl        1/1       Running   0          1h
```

##### 4.3 validate redis
Test redis is working:

```sh
    kubectl -n msa-demo exec -it `kubectl -n msa-demo get pod | grep redis | awk '{print $1}'` -- redis-cli KEYS '*'
```
   should yield:
```
    1) "pings"
```
   (This command picks a pod with 'redis' in its name out of the namespace pod list and execute the command
   `redis-cli KEYS '*'` in it)

   Get the value of pings:
```sh
    kubectl -n msa-demo exec -it `kubectl -n msa-demo get pod | grep redis | awk '{print $1}'` -- redis-cli GET pings
```
   should yield somt number:
```sh
    "2152"
```

##### 4.4 validate msa-api
   Test msa-api is working:

```sh
    kubectl -n msa-demo logs `kubectl -n msa-demo get pod | grep msa-api | awk '{print $1}'`
```

   should yield:

```sh
    > msa-api@1.0.0 start /opt/tikal
    > node api.js

    loading ./config/development.json
    Connecting to cache_host: redis://10.110.76.53:6379
    Server running on port 8080!
    node_redis: Warning: Redis server does not require a password, but a password was supplied.
```
    
   kubectl -n msa-demo logs `kubectl -n msa-demo get pod | grep msa-poller | awk '{print $1}'` -f
    
   (Those commands use a hack to get the pod name from kubectl. we need it because the pod name is generated at runtime and has a random part. )
#### 5. Cleanup
```sh
    kubectl delete namespace msa-demo
```
   Make sure all objects are gone (Now you think what command to run)
