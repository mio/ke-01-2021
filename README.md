# Kubexperience course
## Kubernetes for developers

### Overview & Purpose
The purpose of the Kubexperience course is to provide a high-level overview of Kubernetes and its components to Software Engineers & IT professionals adopting DevOps methodologies.
We will cover the essentials one needs to master, deploy, scale, and serve applications over Kubernetes. 
As an end-user, we will cover the core concepts and how to manage workloads using the Kubernetes CLI (a.k.a kubectl)
Kubernetes YAML based manifests. We will introduce more advanced management of application workload using
the Helm tool, charts, and templates.
We will conclude with a set of common best practices for managing an application life cycle on Kubernetes.

### Target audience:
This workshop introduces Kubernetes concepts to
software developers with working experience Docker, who want to use Kubernetes to orchestrate their dockerized workload.
This workshop will also suit DevOps professionals who have experience with Docker, Docker Swarm that want to get started with Kubernetes.

All participants should have a good understanding of the Linux operating system, at least basic knowledge of Docker, and some scripting and command-line experience to better get along the hands-on sections.


### Prerequisites
* The following tools should be installed:
    * minikube -> https://kubernetes.io/docs/tasks/tools/install-minikube/
       OR
    * Docker for Mac with kubernetes enabled -> https://docs.docker.com/docker-for-mac/install/ and 
    * kubectl -> https://kubernetes.io/docs/tasks/tools/install-kubectl/ 

### Syllabus

1. Introduction to Kubernetes:
    * Kubernetes architecture and key concepts
    * Logical layout of Kubernetes configuration object
    * The kubectl tool
2. Deploying application to Kubernetes with native YAML manifests
    * Create and edit YAML manifests for deployments and services
    * Persist storage, configuration injection
    * multi-(micro-)services system
3. Some advance Kubernetes objects
    * ingress and ingress controller
    * RBAC access control system.
    * HPA
    * CRD
4. Helm tool, charts, and templates
    * Working with helm
    * templates and values
    * The umbrella pattern
